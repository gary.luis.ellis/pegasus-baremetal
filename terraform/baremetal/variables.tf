variable "ssh_user"{
  description = "the ssh user"
  default = ""
}

variable "control_plane_nodes" {
  description = "a list of control plane nodes"
  type = list(map(string))
}

variable "worker_nodes" {
  description = "a list of worker nodes"
  type = list(map(string))
  default = []
}
