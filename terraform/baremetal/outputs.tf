output "control_plane_nodes" {
  value = [
    for i in var.control_plane_nodes[*]:
      {
        user        = var.ssh_user
        hostname    = i.hostname
        address     = i.address
        labels      = { "node-role.kubernetes.io/etcd" = "" }
      }
  ]
}

output "worker_nodes" {
  value = [
    for i in var.worker_nodes[*]:
      {
        user        = var.ssh_user
        hostname    = i.hostname
        address     = i.address
        labels      = { "node-role.kubernetes.io/worker" = "" }
      }
  ]
}
